package br.com.facisa.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "tb_empresa")
@SequenceGenerator(
    sequenceName = "tb_empresa_id_empresa_seq",
    name = "tb_empresa_id_empresa_seq",
    allocationSize = 1)
public class Empresa implements Serializable {

  private static final long serialVersionUID = 3259326014001072183L;

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tb_empresa_id_empresa_seq")
  @Column(name = "id_empresa", nullable = false)
  private Long id;

  @Column(name = "ds_razao_social", nullable = false)
  private String razaoSocial;

  @Column(name = "dt_cadastro", nullable = false)
  @Temporal(TemporalType.TIMESTAMP)
  private Date dataCadastro;

  @OneToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "fk_atividade", updatable = false, nullable = false)
  private Atividade atividade;

  @OneToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "fk_contribuinte", updatable = false, nullable = false)
  private Contribuinte contribuinte;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getRazaoSocial() {
    return razaoSocial;
  }

  public void setRazaoSocial(String razaoSocial) {
    this.razaoSocial = razaoSocial;
  }

  public Date getDataCadastro() {
    return dataCadastro;
  }

  public void setDataCadastro(Date dataCadastro) {
    this.dataCadastro = dataCadastro;
  }

  public Atividade getAtividade() {
    return atividade;
  }

  public void setAtividade(Atividade atividade) {
    this.atividade = atividade;
  }

  public Contribuinte getContribuinte() {
    return contribuinte;
  }

  public void setContribuinte(Contribuinte contribuinte) {
    this.contribuinte = contribuinte;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Empresa empresa = (Empresa) o;
    return Objects.equals(id, empresa.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
