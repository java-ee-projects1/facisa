package br.com.facisa.model.dto.contribuinte;

import br.com.facisa.model.dto.empresa.EmpresaNovoDto;
import io.swagger.annotations.ApiModel;
import org.hibernate.validator.constraints.br.CNPJ;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@ApiModel(value = "ContribuinteNovoDto", description = "Classe com informações do contruibuinte")
public class ContribuinteNovoDto implements Serializable {

  private static final long serialVersionUID = 788420403292566451L;

  @NotBlank(message = "O campo Nome é obrigatório")
  private String nome;

  @CNPJ
  @NotBlank(message = "O campo CNPJ é obrigatório")
  private String cnpjcpf;

  private EmpresaNovoDto empresaNovoDto;

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getCnpjcpf() {
    return cnpjcpf;
  }

  public void setCnpjcpf(String cnpjcpf) {
    this.cnpjcpf = cnpjcpf;
  }

  public EmpresaNovoDto getEmpresaNovoDto() {
    return empresaNovoDto;
  }

  public void setEmpresaNovoDto(EmpresaNovoDto empresaNovoDto) {
    this.empresaNovoDto = empresaNovoDto;
  }
}
