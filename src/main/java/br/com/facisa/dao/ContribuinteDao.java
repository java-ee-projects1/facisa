package br.com.facisa.dao;

import br.com.facisa.exception.FacisaException;
import br.com.facisa.model.Contribuinte;
import br.com.facisa.model.dto.contribuinte.ContribuinteDto;

import javax.ejb.Local;
import java.util.List;

@Local
public interface ContribuinteDao {

  List<ContribuinteDto> consultarContribuinte() throws FacisaException;

  Contribuinte salvarContribuinte(Contribuinte contribuinte) throws FacisaException;

  Contribuinte consultarPorId(Long idContribuinte) throws FacisaException;

  Contribuinte editarContribuinte(Contribuinte contribuinte) throws FacisaException;
}
