package br.com.facisa.service;

import br.com.facisa.exception.FaciaNegocioException;
import br.com.facisa.exception.FacisaException;
import br.com.facisa.model.Atividade;

import javax.ejb.Local;

@Local
public interface AtividadeService {

  Atividade consultartPorDescricao(String descricao) throws FaciaNegocioException, FacisaException;
}
