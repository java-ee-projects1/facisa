package br.com.facisa.exception;

import br.com.facisa.util.ConstantesUtil;

public class FacisaException extends RuntimeException {

  public FacisaException() {
    super(ConstantesUtil.ERRO_AO_ACESSAR_A_BASE_DE_DADOS);
  }

  public FacisaException(String mensagem) {
    super(mensagem);
  }
}
