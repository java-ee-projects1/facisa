package br.com.facisa.model.dto.contribuinte;

import br.com.facisa.model.dto.empresa.EmpresaEdicaoDto;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

public class ContribuinteEditacaoDto implements Serializable {

  private static final long serialVersionUID = 4460990813271815599L;

  @NotBlank(message = "O campo Nome é obrigatório")
  private String nome;

  private EmpresaEdicaoDto empresaEdicaoDto;

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public EmpresaEdicaoDto getEmpresaEdicaoDto() {
    return empresaEdicaoDto;
  }

  public void setEmpresaEdicaoDto(EmpresaEdicaoDto empresaEdicaoDto) {
    this.empresaEdicaoDto = empresaEdicaoDto;
  }
}
