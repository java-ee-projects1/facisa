package br.com.facisa.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "tb_contribuinte")
@SequenceGenerator(
    sequenceName = "tb_contribuinte_id_contribuinte_seq",
    name = "tb_contribuinte_id_contribuinte_seq",
    allocationSize = 1)
public class Contribuinte implements Serializable {

  private static final long serialVersionUID = 3259326014001072183L;

  @Id
  @GeneratedValue(
      strategy = GenerationType.SEQUENCE,
      generator = "tb_contribuinte_id_contribuinte_seq")
  @Column(name = "id_contribuinte")
  private Long id;

  @Column(name = "nome")
  private String nome;

  @Column(name = "nu_cnpj_cpf")
  private String cnpjcpf;

  @Column(name = "fl_status")
  private boolean status;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getCnpjcpf() {
    return cnpjcpf;
  }

  public void setCnpjcpf(String cnpjcpf) {
    this.cnpjcpf = cnpjcpf;
  }

  public boolean isStatus() {
    return status;
  }

  public void setStatus(boolean status) {
    this.status = status;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Contribuinte that = (Contribuinte) o;
    return id.equals(that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
