package br.com.facisa.dateDeserializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Esta classe tem como objetivo converter uma {@link LocalDateTime} para uma classe {@link String}
 * para disponibilizar formatação no objeto json
 */
public class JsonDateSerializer extends JsonSerializer<LocalDateTime> {

  private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

  @Override
  public void serialize(
      LocalDateTime localDateTime,
      JsonGenerator jsonGenerator,
      SerializerProvider serializerProvider)
      throws IOException {
    final String dateString = localDateTime.format(this.formatter);
    jsonGenerator.writeString(dateString);
  }
}
