package br.com.facisa.model.mapper;

import br.com.facisa.model.Empresa;
import br.com.facisa.model.dto.empresa.EmpresaNovoDto;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class EmpresaMapper {

  public static Empresa mapper(EmpresaNovoDto empresaNovoDto) {
    Empresa empresa = new Empresa();
    empresa.setDataCadastro(Timestamp.valueOf(LocalDateTime.now()));
    empresa.setRazaoSocial(empresaNovoDto.getRazaoSocial());

    return empresa;
  }
}
