package br.com.facisa;

import io.swagger.jaxrs.config.BeanConfig;

import javax.servlet.ServletConfig;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;

@ApplicationPath("api/v1")
public class AplicacaoBase extends Application {

  public AplicacaoBase(@Context ServletConfig servletConfig) {
    BeanConfig beanConfig = new BeanConfig();
    beanConfig.setVersion("1.0");
    beanConfig.setTitle("Unifacisa");
    beanConfig.setBasePath("facisa/api/v1");
    beanConfig.setResourcePackage("br.com.facisa");
    beanConfig.setScan(true);
  }
}
