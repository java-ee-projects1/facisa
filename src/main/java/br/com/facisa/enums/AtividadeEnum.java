package br.com.facisa.enums;

import java.util.stream.Stream;

public enum AtividadeEnum {
  FARMACIA("COD_1", "Farmácia");

  private String codigo;
  private String descricao;

  AtividadeEnum(String codigo, String descricao) {
    this.codigo = codigo;
    this.descricao = descricao;
  }

  public String getCodigo() {
    return codigo;
  }

  public String getDescricao() {
    return descricao;
  }

  public static Stream<AtividadeEnum> getAtividadeEnum() {
    return Stream.of(AtividadeEnum.values());
  }
}
