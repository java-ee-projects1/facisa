package br.com.facisa.dao;

import br.com.facisa.model.Empresa;

import javax.ejb.Local;

@Local
public interface EmpresaDao {

  Long editarEmpresa(Empresa empresa);

  Empresa consultarPorId(Long idEmpresa);

  Empresa salvarEmpresa(Empresa empresa);
}
