package br.com.facisa.dao.impl;

import br.com.facisa.dao.AtividadeDao;
import br.com.facisa.exception.FacisaException;
import br.com.facisa.model.Atividade;
import org.hibernate.NonUniqueResultException;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import java.util.stream.Stream;

@Stateless
public class AtividadeDaoImpl implements AtividadeDao {

  private static final String CONSULTA_SEM_DADOS = "Consulta não retornou dados";
  private static final String CONSULTANDO_ATIVIDADE_POR_DESCRICAO =
      "Consultando atividade por descricao";
  private final Logger LOG = LoggerFactory.getLogger(AtividadeDaoImpl.class);

  @PersistenceContext(unitName = "facisaDS")
  private EntityManager entityManager;

  @Override
  public Stream<Atividade> consultarPorDescricao(String descricao) throws FacisaException {

    Query query =
        this.entityManager
            .unwrap(Session.class)
            .createQuery(
                "select a from Atividade a where a.descricao = :descricao", Atividade.class);

    query.setParameter("descricao", descricao);

    try {
      return Stream.of((Atividade) query.getSingleResult());
    } catch (NoResultException | NonUniqueResultException e) {
      LOG.info(CONSULTA_SEM_DADOS, e);
      return Stream.of();
    } catch (PersistenceException e) {
      LOG.error(CONSULTANDO_ATIVIDADE_POR_DESCRICAO, e);
      throw new FacisaException();
    }
  }
}
