package br.com.facisa.service.impl;

import br.com.facisa.dao.EmpresaDao;
import br.com.facisa.exception.FaciaNegocioException;
import br.com.facisa.exception.FacisaException;
import br.com.facisa.model.Atividade;
import br.com.facisa.model.Contribuinte;
import br.com.facisa.model.Empresa;
import br.com.facisa.model.dto.contribuinte.ContribuinteEditacaoDto;
import br.com.facisa.model.dto.empresa.EmpresaEdicaoDto;
import br.com.facisa.model.dto.empresa.EmpresaNovoDto;
import br.com.facisa.model.mapper.EmpresaMapper;
import br.com.facisa.service.AtividadeService;
import br.com.facisa.service.ContribuinteService;
import br.com.facisa.service.EmpresaService;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

@Stateless
@TransactionAttribute(value = TransactionAttributeType.NOT_SUPPORTED)
public class EmpresaServiceImpl implements EmpresaService {

  @EJB private EmpresaDao empresaDao;

  @EJB private ContribuinteService contribuinteService;

  @EJB private AtividadeService atividadeService;

  @Override
  @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
  public Empresa salvarEditar(Contribuinte contribuinte, EmpresaNovoDto empresaNovoDto)
      throws FacisaException, FaciaNegocioException {

    Empresa empresa = EmpresaMapper.mapper(empresaNovoDto);
    empresa.setContribuinte(contribuinte);
    empresa.setAtividade(
        this.atividadeService.consultartPorDescricao(empresaNovoDto.getAtividade()));
    return this.empresaDao.salvarEmpresa(empresa);
  }

  @Override
  @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
  public Long editarEmpresa(Long id, ContribuinteEditacaoDto contribuinteEditacaoDto)
      throws FacisaException, FaciaNegocioException {

    Atividade atividade =
        this.atividadeService.consultartPorDescricao(
            contribuinteEditacaoDto.getEmpresaEdicaoDto().getAtividade());

    Empresa empresa =
        this.empresaDao.consultarPorId(
            contribuinteEditacaoDto.getEmpresaEdicaoDto().getIdEmpresa());

    if (verificarSeHouveAlteracaoEmpresa(
        empresa, atividade, contribuinteEditacaoDto.getEmpresaEdicaoDto())) {
      return empresaDao.editarEmpresa(empresa);
    }

    return empresa.getId();
  }

  private boolean verificarSeHouveAlteracaoEmpresa(
      Empresa empresa, Atividade atividade, EmpresaEdicaoDto empresaEdicaoDto) {

    boolean houveAlteracao = false;

    if (!atividade.getDescricao().equalsIgnoreCase(empresaEdicaoDto.getAtividade())) {
      empresa.setAtividade(atividade);
      houveAlteracao = true;
    }

    if (!empresa.getRazaoSocial().equalsIgnoreCase(empresaEdicaoDto.getRazaoSocial())) {
      empresa.setRazaoSocial(empresaEdicaoDto.getRazaoSocial());
      houveAlteracao = true;
    }
    return houveAlteracao;
  }
}
