package br.com.facisa.service.impl;

import br.com.facisa.dao.ContribuinteDao;
import br.com.facisa.exception.FaciaNegocioException;
import br.com.facisa.exception.FacisaException;
import br.com.facisa.model.Contribuinte;
import br.com.facisa.model.dto.contribuinte.ContribuinteDto;
import br.com.facisa.model.dto.contribuinte.ContribuinteEditacaoDto;
import br.com.facisa.model.dto.contribuinte.ContribuinteNovoDto;
import br.com.facisa.model.mapper.ContribuinteMapper;
import br.com.facisa.service.ContribuinteService;
import br.com.facisa.service.EmpresaService;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import java.util.List;

@Stateless
@TransactionAttribute(value = TransactionAttributeType.NOT_SUPPORTED)
public class ContribuinteServiceImpl implements ContribuinteService {

  @EJB private ContribuinteDao contribuinteDao;

  @EJB private EmpresaService empresaService;

  public List<ContribuinteDto> consultarContribuinte() throws FacisaException {
    return this.contribuinteDao.consultarContribuinte();
  }

  @Override
  @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
  public Long salvarContribuinte(ContribuinteNovoDto contribuinteNovoDto)
      throws FacisaException, FaciaNegocioException {
    Contribuinte contribuinte = ContribuinteMapper.mapper(contribuinteNovoDto);
    Contribuinte contribuinteBase = this.contribuinteDao.salvarContribuinte(contribuinte);
    this.empresaService.salvarEditar(contribuinteBase, contribuinteNovoDto.getEmpresaNovoDto());
    return contribuinteBase.getId();
  }

  @Override
  public Contribuinte consultarContribuinteId(Long idContribuinte) throws FacisaException {
    return this.contribuinteDao.consultarPorId(idContribuinte);
  }

  @Override
  @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
  public Long editarContribuinte(Long id, ContribuinteEditacaoDto contribuinteEditacaoDto)
      throws FacisaException, FaciaNegocioException {

    Contribuinte contribuinte = this.contribuinteDao.consultarPorId(id);
    contribuinte.setNome(contribuinteEditacaoDto.getNome());
    this.contribuinteDao.editarContribuinte(contribuinte);
    this.empresaService.editarEmpresa(id, contribuinteEditacaoDto);

    return contribuinte.getId();
  }

  @Override
  @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
  public Long deletarContribuinte(Long idContribuinte) throws FacisaException {
    Contribuinte contribuinte = this.contribuinteDao.consultarPorId(idContribuinte);
    contribuinte.setStatus(false);
    return contribuinte.getId();
  }
}
