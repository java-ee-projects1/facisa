package br.com.facisa.model.mapper;

import br.com.facisa.model.Contribuinte;
import br.com.facisa.model.dto.contribuinte.ContribuinteNovoDto;

public class ContribuinteMapper {

  public static Contribuinte mapper(ContribuinteNovoDto contribuinteNovoDto) {

    Contribuinte contribuinte = new Contribuinte();
    contribuinte.setStatus(true);
    contribuinte.setNome(contribuinteNovoDto.getNome());
    contribuinte.setCnpjcpf(contribuinteNovoDto.getCnpjcpf());

    return contribuinte;
  }
}
