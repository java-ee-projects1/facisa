package br.com.facisa.service.impl;

import br.com.facisa.dao.AtividadeDao;
import br.com.facisa.exception.FaciaNegocioException;
import br.com.facisa.exception.FacisaException;
import br.com.facisa.model.Atividade;
import br.com.facisa.service.AtividadeService;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

@Stateless
@TransactionAttribute(value = TransactionAttributeType.NOT_SUPPORTED)
public class AtividadeServiceImpl implements AtividadeService {

  @EJB AtividadeDao atividadeDao;

  @Override
  public Atividade consultartPorDescricao(String descricao)
      throws FaciaNegocioException, FacisaException {
    return this.atividadeDao
        .consultarPorDescricao(descricao)
        .findFirst()
        .orElseThrow(() -> new FaciaNegocioException("Atividade não encontrada"));
  }
}
