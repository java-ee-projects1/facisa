package br.com.facisa.exception;

import javax.ejb.ApplicationException;

@ApplicationException(rollback = true)
public class FaciaNegocioException extends Exception {

  public FaciaNegocioException(String mensagem) {
    super(mensagem);
  }
}
