package br.com.facisa.model.dto.empresa;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class EmpresaEdicaoDto extends EmpresaNovoDto implements Serializable {

  private static final long serialVersionUID = -1444454665920245256L;

  @NotNull(message = "O campo Id da Empersa é obrigatório")
  private Long idEmpresa;

  public Long getIdEmpresa() {
    return idEmpresa;
  }

  public void setIdEmpresa(Long idEmpresa) {
    this.idEmpresa = idEmpresa;
  }
}
