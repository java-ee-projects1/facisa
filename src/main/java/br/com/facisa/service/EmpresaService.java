package br.com.facisa.service;

import br.com.facisa.exception.FaciaNegocioException;
import br.com.facisa.exception.FacisaException;
import br.com.facisa.model.Contribuinte;
import br.com.facisa.model.Empresa;
import br.com.facisa.model.dto.contribuinte.ContribuinteEditacaoDto;
import br.com.facisa.model.dto.empresa.EmpresaNovoDto;

import javax.ejb.Local;

@Local
public interface EmpresaService {

  Long editarEmpresa(Long id, ContribuinteEditacaoDto contribuinteEditacaoDto)
      throws FacisaException, FaciaNegocioException;

  Empresa salvarEditar(Contribuinte contribuinte, EmpresaNovoDto empresaNovoDto)
      throws FacisaException, FaciaNegocioException;
}
