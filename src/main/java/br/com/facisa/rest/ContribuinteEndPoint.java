package br.com.facisa.rest;

import br.com.facisa.exception.FaciaNegocioException;
import br.com.facisa.exception.FacisaException;
import br.com.facisa.model.dto.contribuinte.ContribuinteDto;
import br.com.facisa.model.dto.contribuinte.ContribuinteEditacaoDto;
import br.com.facisa.model.dto.contribuinte.ContribuinteNovoDto;
import io.swagger.annotations.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Api(tags = "Contribuintes")
@Path("contribuintes")
public interface ContribuinteEndPoint {

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @ApiOperation(
      value = "Listagem de contruibuintes ativos pela data de inicio da empresa.",
      response = ContribuinteDto.class,
      responseContainer = "List")
  @ApiResponses(
      value = {
        @ApiResponse(code = 400, message = "Exceção de negócio do sistema"),
        @ApiResponse(code = 500, message = "Erro Interno da API"),
        @ApiResponse(code = 404, message = "Recurso não encontrado")
      })
  Response consultarContribuinte() throws FacisaException;

  @POST
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  @ApiOperation(
      value = "Salvar o contribuinte e empresa no sistema",
      response = ContribuinteDto.class)
  @ApiResponses(
      value = {
        @ApiResponse(code = 400, message = "Exceção de negócio do sistema"),
        @ApiResponse(code = 500, message = "Erro Interno da API"),
        @ApiResponse(code = 404, message = "Recurso não encontrado")
      })
  Response salvarContribuinte(
      @ApiParam(value = "Objeto ContribuinteNovoDto esperado", required = true) @Valid
          ContribuinteNovoDto contribuinte)
      throws FacisaException, FaciaNegocioException;

  @PUT
  @Path("{id}")
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  @ApiOperation(
      value = "Editar o contribuinte e empresa no sistema",
      response = ContribuinteDto.class)
  @ApiResponses(
      value = {
        @ApiResponse(code = 400, message = "Exceção de negócio do sistema"),
        @ApiResponse(code = 500, message = "Erro Interno da API"),
        @ApiResponse(code = 404, message = "Recurso não encontrado")
      })
  Response editarContribuinte(
      @ApiParam(value = "Id do Contribuinte", required = true) @NotNull @PathParam("id")
          Long idContribuinte,
      @ApiParam(value = "Objeto ContribuinteEditacaoDto esperado", required = true) @Valid
          ContribuinteEditacaoDto contribuinte)
      throws FacisaException, FaciaNegocioException;

  @DELETE
  @Path("{id}")
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  @ApiOperation(value = "Inativar o contribuinte do sistema", response = ContribuinteDto.class)
  @ApiResponses(
      value = {
        @ApiResponse(code = 400, message = "Exceção de negócio do sistema"),
        @ApiResponse(code = 500, message = "Erro Interno da API"),
        @ApiResponse(code = 404, message = "Recurso não encontrado")
      })
  Response deletarContribuinte(
      @ApiParam(value = "Id do Contribuinte", required = true) @NotNull @PathParam("id")
          Long idContribuinte)
      throws FacisaException;
}
