package br.com.facisa.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * Esta classe tem como objetivo executar a conversao de tipos de dados {@link Timestamp} para
 * {@link LocalDateTime} mapeado para uma entidade que representa uma tabela no banco de dados
 */
@Converter
public class LocalDatimeConverter implements AttributeConverter<Timestamp, LocalDateTime> {

  @Override
  public LocalDateTime convertToDatabaseColumn(Timestamp attribute) {
    return attribute.toLocalDateTime();
  }

  @Override
  public Timestamp convertToEntityAttribute(LocalDateTime dbData) {
    return Timestamp.valueOf(dbData);
  }
}
