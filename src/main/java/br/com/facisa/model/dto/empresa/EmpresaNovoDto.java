package br.com.facisa.model.dto.empresa;

import io.swagger.annotations.ApiModel;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@ApiModel(
    value = "EmpresaNovoDto",
    description = "Classe com informações da empresa do contruibuinte")
public class EmpresaNovoDto implements Serializable {

  private static final long serialVersionUID = -6540972626339751430L;

  @NotBlank(message = "O campo Razaão Social é obrigatório")
  private String razaoSocial;

  @NotBlank(message = "O campo Atividade é obrigatório")
  private String atividade;

  public String getRazaoSocial() {
    return razaoSocial;
  }

  public void setRazaoSocial(String razaoSocial) {
    this.razaoSocial = razaoSocial;
  }

  public String getAtividade() {
    return atividade;
  }

  public void setAtividade(String atividade) {
    this.atividade = atividade;
  }
}
