package br.com.facisa.rest.impl;

import br.com.facisa.exception.FaciaNegocioException;
import br.com.facisa.exception.FacisaException;
import br.com.facisa.model.dto.contribuinte.ContribuinteEditacaoDto;
import br.com.facisa.model.dto.contribuinte.ContribuinteNovoDto;
import br.com.facisa.rest.ContribuinteEndPoint;
import br.com.facisa.service.ContribuinteService;

import javax.ejb.EJB;
import javax.ws.rs.core.Response;

public class ContribuinteEndPointImpl implements ContribuinteEndPoint {

  @EJB private ContribuinteService contribuinteService;

  @Override
  public Response consultarContribuinte() throws FacisaException {
    return Response.ok(contribuinteService.consultarContribuinte()).build();
  }

  @Override
  public Response salvarContribuinte(ContribuinteNovoDto contribuinteNovoDto)
      throws FacisaException, FaciaNegocioException {
    return Response.ok(this.contribuinteService.salvarContribuinte(contribuinteNovoDto)).build();
  }

  @Override
  public Response editarContribuinte(
      Long idContribuinte, ContribuinteEditacaoDto contribuinteEditacaoDto)
      throws FacisaException, FaciaNegocioException {
    return Response.ok(
            this.contribuinteService.editarContribuinte(idContribuinte, contribuinteEditacaoDto))
        .build();
  }

  @Override
  public Response deletarContribuinte(Long idContribuinte) throws FacisaException {
    return Response.ok(this.contribuinteService.deletarContribuinte(idContribuinte)).build();
  }
}
