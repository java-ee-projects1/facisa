package br.com.facisa.dao.impl;

import br.com.facisa.dao.EmpresaDao;
import br.com.facisa.exception.FacisaException;
import br.com.facisa.model.Empresa;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

@Stateless
public class EmpresaDaoImpl implements EmpresaDao {

  private static final String EDITANDO_UMA_EMPRESA = "Editando uma empresa";
  private static final String CONSULTANDO_EMPRESA_POR_ID = "Consultando empresa por id";
  private static final String SALVANDO_UMA_EMPRESA = "Salvando uma empresa";
  private final Logger LOG = LoggerFactory.getLogger(EmpresaDaoImpl.class);

  @PersistenceContext(unitName = "facisaDS")
  private EntityManager entityManager;

  @Override
  public Long editarEmpresa(Empresa empresa) {

    try {
      Empresa empresabase = (Empresa) this.entityManager.unwrap(Session.class).merge(empresa);
      return empresabase.getId();
    } catch (PersistenceException e) {
      LOG.error(EDITANDO_UMA_EMPRESA, e);
      throw new FacisaException();
    }
  }

  @Override
  public Empresa consultarPorId(Long idEmpresa) {

    try {
      return this.entityManager.unwrap(Session.class).find(Empresa.class, idEmpresa);
    } catch (PersistenceException e) {
      LOG.error(CONSULTANDO_EMPRESA_POR_ID, e);
      throw new FacisaException();
    }
  }

  @Override
  public Empresa salvarEmpresa(Empresa empresa) {

    try {
      this.entityManager.unwrap(Session.class).save(empresa);
      return consultarPorId(empresa.getId());
    } catch (PersistenceException e) {
      LOG.error(SALVANDO_UMA_EMPRESA, e);
      throw new FacisaException();
    }
  }
}
