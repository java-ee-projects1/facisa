package br.com.facisa.dao.impl;

import br.com.facisa.dao.ContribuinteDao;
import br.com.facisa.exception.FacisaException;
import br.com.facisa.model.Contribuinte;
import br.com.facisa.model.dto.contribuinte.ContribuinteDto;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import java.util.List;

@Stateless
public class ContribuinteDaoImpl implements ContribuinteDao {

  private static final String CONSULTANDO_CONTRIBUINTE = "Consultando contribuinte";
  private static final String SALVANDO_CONTRUIBUINTE = "Salvando contruibuinte";
  private static final String CONSULTANDO_CONTRUIBUINTE_POR_ID = "Consultando contruibuinte por id";
  private static final String EDITANDO_CONTRUIBUINTE = "Editando contruibuinte";
  private final Logger LOG = LoggerFactory.getLogger(ContribuinteDaoImpl.class);

  @PersistenceContext(unitName = "facisaDS")
  private EntityManager entityManager;

  @Override
  public List<ContribuinteDto> consultarContribuinte() throws FacisaException {

    try {

      String consulta = " select c.id_contribuinte as id, " + "       c.nome            as nome, "
          + "       c.nu_cnpj_cpf     as cnpjcpf, "
          + "       te.ds_razao_social as \"razaoSocial\" , "
          + "       te.dt_cadastro    as \"dataCadastro\" , " + "       ta.ds_nome as atividade "
          + " from public.tb_contribuinte c "
          + "         inner join tb_empresa te on c.id_contribuinte = te.fk_contribuinte "
          + "         inner join tb_atividade ta on te.fk_atividade = ta.id_atividade "
          + " where c.fl_status = true " + " order by c.nome asc ";

      return this.entityManager
          .unwrap(Session.class)
          .createNativeQuery(consulta)
          .setResultTransformer(Transformers.aliasToBean(ContribuinteDto.class))
          .list();
    } catch (PersistenceException e) {
      LOG.error(CONSULTANDO_CONTRIBUINTE, e);
      throw new FacisaException();
    }
  }

  @Override
  public Contribuinte salvarContribuinte(Contribuinte contribuinte) throws FacisaException {

    try {
      this.entityManager.unwrap(Session.class).save(contribuinte);
      return this.entityManager
          .unwrap(Session.class)
          .find(Contribuinte.class, contribuinte.getId());
    } catch (PersistenceException e) {
      LOG.error(SALVANDO_CONTRUIBUINTE, e);
      throw new FacisaException();
    }
  }

  @Override
  public Contribuinte consultarPorId(Long idContribuinte) throws FacisaException {
    try {
      return this.entityManager.unwrap(Session.class).find(Contribuinte.class, idContribuinte);
    } catch (PersistenceException e) {
      LOG.error(CONSULTANDO_CONTRUIBUINTE_POR_ID, e);
      throw new FacisaException();
    }
  }

  @Override
  public Contribuinte editarContribuinte(Contribuinte contribuinte) throws FacisaException {

    try {
      this.entityManager.unwrap(Session.class).update(contribuinte);
      return this.entityManager
          .unwrap(Session.class)
          .find(Contribuinte.class, contribuinte.getId());
    } catch (PersistenceException e) {
      LOG.error(EDITANDO_CONTRUIBUINTE, e);
      throw new FacisaException();
    }
  }
}
