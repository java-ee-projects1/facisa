package br.com.facisa.dao;

import br.com.facisa.exception.FacisaException;
import br.com.facisa.model.Atividade;

import javax.ejb.Local;
import java.util.stream.Stream;

@Local
public interface AtividadeDao {

  Stream<Atividade> consultarPorDescricao(String descricao) throws FacisaException;
}
