package br.com.facisa.service;

import br.com.facisa.exception.FaciaNegocioException;
import br.com.facisa.exception.FacisaException;
import br.com.facisa.model.Contribuinte;
import br.com.facisa.model.dto.contribuinte.ContribuinteDto;
import br.com.facisa.model.dto.contribuinte.ContribuinteEditacaoDto;
import br.com.facisa.model.dto.contribuinte.ContribuinteNovoDto;

import javax.ejb.Local;
import java.util.List;

@Local
public interface ContribuinteService {

  List<ContribuinteDto> consultarContribuinte() throws FacisaException;

  Long salvarContribuinte(ContribuinteNovoDto contribuinte)
      throws FacisaException, FaciaNegocioException;

  Contribuinte consultarContribuinteId(Long idContribuinte) throws FacisaException;

  Long editarContribuinte(Long id, ContribuinteEditacaoDto contribuinteEditacaoDto)
      throws FacisaException, FaciaNegocioException;

  Long deletarContribuinte(Long idContribuinte) throws FacisaException;
}
