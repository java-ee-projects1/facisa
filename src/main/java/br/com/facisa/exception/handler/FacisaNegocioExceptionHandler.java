package br.com.facisa.exception.handler;

import br.com.facisa.exception.FaciaNegocioException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class FacisaNegocioExceptionHandler implements ExceptionMapper<FaciaNegocioException> {

  @Override
  public Response toResponse(FaciaNegocioException exception) {
    return Response.status(Response.Status.BAD_REQUEST)
        .type(MediaType.APPLICATION_JSON)
        .entity(exception.getMessage())
        .build();
  }
}
