package br.com.facisa.model.dto.contribuinte;

import io.swagger.annotations.ApiModel;

import java.io.Serializable;
import java.sql.Timestamp;

@ApiModel(value = "ContribuinteDto", description = "Classe com informações do contruibuinte")
public class ContribuinteDto implements Serializable {

  private static final long serialVersionUID = 6447153682506625189L;

  private Integer id;
  private String nome;
  private String cnpjcpf;
  private String razaoSocial;
  private Timestamp dataCadastro;
  private String atividade;

  public ContribuinteDto() {}

  public ContribuinteDto(
      Integer id,
      String nome,
      String cnpjcpf,
      String razaoSocial,
      Timestamp dataCadastro,
      String atividade) {
    this.id = id;
    this.nome = nome;
    this.razaoSocial = razaoSocial;
    this.cnpjcpf = cnpjcpf;
    this.dataCadastro = dataCadastro;
    this.atividade = atividade;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getCnpjcpf() {
    return cnpjcpf;
  }

  public void setCnpjcpf(String cnpjcpf) {
    this.cnpjcpf = cnpjcpf;
  }

  public Timestamp getDataCadastro() {
    return dataCadastro;
  }

  public void setDataCadastro(Timestamp dataCadastro) {
    this.dataCadastro = dataCadastro;
  }

  public String getRazaoSocial() {
    return razaoSocial;
  }

  public void setRazaoSocial(String razaoSocial) {
    this.razaoSocial = razaoSocial;
  }

  public String getAtividade() {
    return atividade;
  }

  public void setAtividade(String atividade) {
    this.atividade = atividade;
  }
}
