package br.com.facisa.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "tb_atividade")
@SequenceGenerator(
    sequenceName = "tb_atividade_id_atividade_seq",
    name = "tb_atividade_id_atividade_seq",
    allocationSize = 1)
public class Atividade implements Serializable {

  private static final long serialVersionUID = -4539353276507058159L;

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tb_atividade_id_atividade_seq")
  @Column(name = "id_atividade")
  private Long id;

  @Column(name = "ds_nome")
  private String descricao;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getDescricao() {
    return descricao;
  }

  public void setDescricao(String razaoSocial) {
    this.descricao = razaoSocial;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Atividade atividade = (Atividade) o;
    return id.equals(atividade.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
